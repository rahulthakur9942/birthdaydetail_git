﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MessageSetting.aspx.cs" Inherits="MessageSetting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->
    <link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

    <style>
        th {
            text-align: center;
        }

        td {
            text-align: center;
        }
    </style>
</head>
<body>

    <form id="form1" runat="server">

        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container">
            <div id="DivNew" visible="true" runat="server">
                <div class="card card-register mx-auto mt-5" style="background-color: #2b968c">
                    <div class="card-header">Message Setting</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ContentPlaceHolder1_txtBirthday"> Birthday Msg</label>
                                    <asp:TextBox ID="txtBirthday" runat="server" TextMode="MultiLine" AutoComplete="off" class="form-control" required="true" Height="100px"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ContentPlaceHolder1_txtAnniversary">Anniversary Msg</label>
                                    <asp:TextBox ID="txtAnniversary" runat="server" TextMode="MultiLine" AutoComplete="off" class="form-control" Height="100px"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <asp:Button ID="Submit" runat="server" class="btn btn-primary btn-block" Text="Submit" OnClick="Submit_Click"></asp:Button>
                                </div>
                            </div>
                            
                        </div>
                           <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GVbirthday" runat="server" AutoGenerateColumns="False" BackColor="White" OnRowCommand="GVbirthday_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="Birthday_Message" HeaderText="Birthday Msg" />
                                            <asp:BoundField DataField="Anniversary_Message" HeaderText="Anniversary Msg" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="Select"  CommandArgument='<%#Eval("id") %>'  Font-Bold="True" ForeColor="Blue"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="del"  CommandArgument='<%#Eval("id") %>' Font-Bold="True" ForeColor="Blue"><i class="fa fa-trash"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
         
        </div>
    </form>
</body>
<!-- Bootstrap core JavaScript-->
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js"></script>

<!-- Custom scripts for all pages-->
<script src="js/sb-admin.min.js"></script>
<!-- Demo scripts for this page-->
<script src="js/demo/datatables-demo.js"></script>
<script src="js/demo/chart-area-demos.js"></script>




</html>
