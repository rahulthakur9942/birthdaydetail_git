﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.IO;

public partial class TodayBirthdaysList : System.Web.UI.Page
{
    SqlConnection con;
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        if (IsPostBack == false)
        {
            GridBind();
        }
    }
     public void GridBind()
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_birthday", con);
        cmd.Parameters.AddWithValue("@req", "TodayBirthday");
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            Gvdata.DataSource = dt;
            Gvdata.DataBind();
        }
        con.Close();
    }
    private void ExportGridToExcel()
    {
 
        Response.Clear();
        Response.Buffer = true;
        Response.ClearContent();
        Response.ClearHeaders();
        Response.Charset = "";
        string FileName = "BirthDayList" + DateTime.Now + ".xls";
        StringWriter strwritter = new StringWriter();
        HtmlTextWriter htmltextwrtter = new HtmlTextWriter(strwritter);
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);

        Gvdata.GridLines = GridLines.Both;
        Gvdata.HeaderStyle.Font.Bold = true;
        Gvdata.RenderControl(htmltextwrtter);
        Response.Write(strwritter.ToString());
        Response.End();


    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //required to avoid the runtime error "  
        //Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."  
    }
    protected void Gvdata_PreRender(object sender, EventArgs e)
    {
        GridBind();

        if (Gvdata.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            Gvdata.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            Gvdata.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            Gvdata.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected void btn_export_Click(object sender, EventArgs e)
    {
        ExportGridToExcel();
    }
}