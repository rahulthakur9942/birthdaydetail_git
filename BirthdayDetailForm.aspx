﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BirthdayDetailForm.aspx.cs" Inherits="BirthdayDetailForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->


    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  

 <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    
   


    <style>
           #GVbirthday_length {
          display:none
      }
        th {
            text-align: center;
        }

        td {
            text-align: center;
        }

        .Labelcss
        {
            margin-left:70px;
        }

        #ul_months li {
    display: inline;
}
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container" style="margin-top: -48px; padding-left:0px; padding-right:0px;">
            <div id="DivNew" visible="true" runat="server">
                <div class="card card-register mx-auto mt-5" style="background-color: #2b968c">
                    <div class="card-header">Manage Birthday   <asp:Label ID="Labeltotal" runat="server" CssClass="Labelcss"  Font-Bold="True"  Width="100px"></asp:Label></div>
                    <div class="card-body">
                        <div class="row row-space">
                            <div class="col-12" style="padding: 0;">
                                       <div>
                                 <div class="table-responsive">

                            <table id="ul_months" style="text-align: center;background-color: bisque;"  class="table table-bordered table-striped">
                                <tr>
                                <td id="1"><a href="#">Jan</a></td>
                                <td id="2"><a href="#">Feb</a></td>
                                <td id="3"><a href="#">Mar</a></td>
                              <td id="4"><a href="#">Apr</a></td>
                                <td id="5"><a href="#">May</a></td>
                                <td id="6"><a href="#">Jun</a></td>  </tr>
                                <tr>
                              <td id="7"><a href="#">Jul</a></td>
                                <td id="8"><a href="#">Aug</a></td>
                                <td id="9"><a href="#">Sep</a></td>
                              <td id="10"><a href="#">Oct</a></td>
                                <td id="11"><a href="#">Nov</a></td>
                                <td id="12"><a href="#">Dec</a></td>

                                </tr></table>

                             </div>
                                           </div>
                                </div>
                                 </div>
                        <div class="row">
                                 <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ContentPlaceHolder1_txtDOB">DOB</label>
                                    <asp:TextBox ID="txtDOB" runat="server" ClientIDMode="Static" AutoComplete="off" class="form-control"></asp:TextBox>

                                </div>
                            </div>
                               <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ContentPlaceHolder1_txtContectNumber">Contact Number</label>
                                    <label for="ContentPlaceHolder1_txtContectNumber"  id="lblcontactrange_error" style="color: red;font-size: smaller;font-weight: bold;"></label>
                                    <asp:TextBox ID="txtContectNumber" TextMode="Number" runat="server" AutoComplete="off" ClientIDMode="Static" class="form-control"></asp:TextBox>
                                      <asp:RequiredFieldValidator ID="RequiredFieldValidator2"  ValidationGroup="chkval" ForeColor="Red" ControlToValidate="txtContectNumber" runat="server" ErrorMessage="*Contact no Required*" Font-Bold="True" Font-Size="Smaller"></asp:RequiredFieldValidator>

                                </div>
                            </div>
                                 <div class="col-md-12" id="dv_exists" style="display:none">
                                <div class="form-group">
                                                       <div class="table-responsive">

                            <table style="text-align: center;" class="table table-bordered table-striped">
                                <thead>
                                    <th>Name</th>
                                    <th>DOB</th>
                                     <th>Edit</th>
                                    <th>Delete</th>

                                </thead>
                                     <tbody id="tbl_gvdisplay">
                                    
                                     </tbody>
                                            

                            </table>

                        </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ContentPlaceHolder1_txtName">Enter Name</label>&nbsp; &nbsp; <label id="lblerror" style="color:red;font-weight: 800;font-size: smaller;"></label>
                                    <asp:TextBox ID="txtName" runat="server" AutoComplete="off" class="form-control" required="true"></asp:TextBox>
                                    <asp:RequiredFieldValidator  ValidationGroup="chkval" ID="RequiredFieldValidator1" ForeColor="Red" ControlToValidate="txtName" runat="server" ErrorMessage="*Name Required*" Font-Bold="True" Font-Size="Smaller"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                             <div class="col-md-6" style="display:none">
                                <div class="form-group">
                                    <label for="ContentPlaceHolder1_txtDOA">DOA</label>
                                    <asp:TextBox ID="txtDOA" runat="server" AutoComplete="off" ClientIDMode="Static" class="form-control"></asp:TextBox>

                                </div>
                            </div>
                        </div>
                
                        <div class="row">
                            <div class="col-md-6" style="display:none">
                                <div class="form-group">
                                    <label for="ContentPlaceHolder1_ddCity">Select City</label>
                                    <asp:DropDownList ID="ddCity" runat="server" class="form-control"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="ContentPlaceHolder1_ddLanguage">Select Language</label>
                                    <asp:DropDownList ID="ddLanguage" runat="server" class="form-control">
                                        <asp:ListItem Value="English" Text="English"></asp:ListItem>
                                  <%--      <asp:ListItem Value="2" Text="Hindi"></asp:ListItem>--%>
                                        <asp:ListItem Value="Punjabi" Text="Punjabi"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Button ID="Submit" ValidationGroup="chkval" runat="server" class="btn btn-primary btn-block"  UseSubmitBehavior="false" Text="Submit" OnClick="Submit_Click" ClientIDMode="Static"></asp:Button>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Button ID="btnSearch" runat="server" class="btn btn-primary btn-block" Text="Search" OnClick="btnSearch_Click" UseSubmitBehavior="False"></asp:Button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="DivEditData" visible="false" runat="server">
                <div class="card card-register mx-auto mt-5" style="background-color: #2b968c">
                    <div class="card-header">Search Birthday</div>
                    <div class="card-body">
                        <div class="row">
                              <div class="table-responsive"  style="height: 91px;">

                            <table id="ul_months2" style="text-align: center;background-color: bisque;"  class="table table-bordered table-striped">
                                <tr>
                                <td>
                                    <asp:LinkButton ID="LinkButton1"  CssClass="1" runat="server" CommandName="jan" OnClick="LinkButton1_Click">Jan</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton2" CssClass="2" runat="server" OnClick="LinkButton1_Click">Feb</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton3"  CssClass="3" runat="server" OnClick="LinkButton1_Click">Mar</asp:LinkButton></td>
                              <td><asp:LinkButton ID="LinkButton4"  CssClass="4" runat="server" OnClick="LinkButton1_Click">Apr</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton5"  CssClass="5" runat="server" OnClick="LinkButton1_Click">May</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton6"  CssClass="6" runat="server" OnClick="LinkButton1_Click">Jun</asp:LinkButton></td>  </tr>
                                <tr>
                              <td><asp:LinkButton ID="LinkButton7"  CssClass="7" runat="server" OnClick="LinkButton1_Click">Jul</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton8"  CssClass="8" runat="server" OnClick="LinkButton1_Click">Aug</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton9"  CssClass="9" runat="server" OnClick="LinkButton1_Click">Sep</asp:LinkButton></td>
                              <td><asp:LinkButton ID="LinkButton10"  CssClass="10" runat="server" OnClick="LinkButton1_Click">Oct</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton11"  CssClass="11" runat="server" OnClick="LinkButton1_Click">Nov</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton12"  CssClass="12" runat="server" OnClick="LinkButton1_Click">Dec</asp:LinkButton></td>

                                </tr></table>

                             </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GVbirthday" runat="server" AutoGenerateColumns="False" OnRowCommand="GVbirthday_RowCommand" OnPreRender="GVbirthday_PreRender">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name/Contact" SortExpression="ddlAirlinePenality">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name")+ "<br/> " + Eval("ContectNumber")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                             <asp:BoundField DataField="DOB" HeaderText="D.O.B" ReadOnly="True" />
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="Select" CommandArgument='<%#Eval("ID") %>' Font-Bold="True" ForeColor="Blue"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="del" CommandArgument='<%#Eval("ID") %>' Font-Bold="True" ForeColor="Blue"><i class="fa fa-trash"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             <asp:HiddenField ID="hdn_id" runat="server" />
              <asp:HiddenField ID="hdn_clickval" runat="server" Value="0" ClientIDMode="Static"/>

        </div>
    </form>
</body>

  

<%--<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>--%>
<script type="text/javascript">
    $(document).ready(function () {
        $("#GVbirthday").dataTable();
    });
</script>
 <%-- <script src="http://code.jquery.com/jquery-1.9.0.js"></script>--%>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        var ClickFlag = 0;
        $('#txtDOB').datepicker(
            {
                dateFormat: 'dd/mm/yy',
                changeMonth: true,
                changeYear: true,
                yearRange: '1960:2020'
            }).attr('readonly', 'readonly');
        
        $('#txtDOA').datepicker({
            dateFormat: 'dd/mm/yy',
            // ignoreReadonly: true,
            // allowInputToggle: true,
            changeMonth: true,
            changeYear: true,
            yearRange: '1960:2020'
        }).attr('readonly', 'readonly');


        $("#GVbirthday").dataTable();
        $(document.body).on('click', '.btneditrec', function (e) {
            var currentrow = $(this).closest('tr');
            lang = currentrow.find('.cls_lang').text();
            id = currentrow.find('.cls_id').text();
            dob = currentrow.find('.cls_dob').text();
            contact = currentrow.find('.cls_contact').text();
            name = currentrow.find('.cls_name').text();

            $("#txtDOB").val(dob);
            $("#txtContectNumber").val(contact);
            $("#txtName").val(name);
            $("#ddLanguage").val(lang);
            $("#hdn_id").val(id);
            $("#dv_exists").hide();
            $("#Submit").val("Update");
            $("#Submit").removeAttr('disabled');
            $("#lblerror").text("");
        });

        $(document.body).on('click', '.btnremove', function (e) {
            if (confirm("Are you sure you?")) {
                var currentRow = $(this).closest("tr");

                id = currentRow.find('.cls_id').text();

                $.ajax({
                    type: "POST",
                    url: "BirthdayDetailForm.aspx/del_rec",
                    contentType: "application/json",
                    data: '{ "id": "' + id + '"}',
                    dataType: "json",
                    success: function () {
                        currentRow.closest('tr').remove();
                        alert("Record deleted Successfully");


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {



                    }

                });
            }
        });
     
        $("#Submit").click(function () {
            if (Page_ClientValidate())
            {
            $(this).val('Saving...');
            $(this).attr('disabled', 'disabled');
            }
        });
        $("#txtName").keyup(function () {

            var contactno = $("#txtContectNumber").val();
            var contactname = $(this).val();

            $.ajax({
                type: "POST",
                url: "BirthdayDetailForm.aspx/chk_contact_dtl",
                contentType: "application/json",
                data: '{ "contactno": "' + contactno + '","contactname": "' + contactname + '"}',
                dataType: "json",
                success: function (data) {
                    var obj = jQuery.parseJSON(data.d);
                    if (obj.rtnval == "1") {

                        if ($('#Submit').val() == 'Submit') {
                            $("#lblerror").text("**Record Already Exist.");
                            $("#Submit").attr('disabled', 'disabled');
                        }
                    }
                    else {
                        $("#lblerror").text("");
                        $("#Submit").removeAttr('disabled');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {



                }

            });



        });


        $("#txtContectNumber").keyup(function () {

            if ($(this).val().length < 10) {
                $("#lblcontactrange_error").text("Invalide No!");
                $("#Submit").attr('disabled', 'disabled');
            }
            else {
                $("#lblcontactrange_error").text(" ");

                $("#Submit").removeAttr('disabled');

            }
            chk_contactno($(this).val());

        });

        function chk_contactno(contact_no) {

            var contactno = $("#txtContectNumber").val();
            $.ajax({
                type: "POST",
                url: "BirthdayDetailForm.aspx/chk_contactno",
                contentType: "application/json",
                data: '{ "contactno": "' + contactno + '"}',
                dataType: "json",
                success: function (data) {
                    var obj = jQuery.parseJSON(data.d);
                    if (contact_no != "") {
                        if (obj.tableoption.length != 0) {


                            $("#tbl_gvdisplay").html(obj.tableoption);
                            $("#dv_exists").show();

                        }
                        else {
                            $("#dv_exists").hide();
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {



                }

            });

        }

        $("#txtDOB").click(function () {
            $("#hdn_clickval").val("1");

        })
        $("#ul_months td").on("click", function () {
            var currentTime = new Date();
            var year = currentTime.getFullYear();
            var month = $(this).attr("id");
            $("#hdn_clickval").val("0");
            $("#txtDOB").val('01/' + month + '/' + year + '');

            //$("#txtDOB").click(function () {
            //    $(".monthselect").val(month - 1);

            //});

            //$("#txtDOB").click();

        });


   
    });
</script>
     <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
      <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

</html>
