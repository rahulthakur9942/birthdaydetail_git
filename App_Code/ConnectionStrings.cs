﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ConnectionStrings
/// </summary>
public static class ConnectionStrings
{
    public static string ConnStrings()
    {
       string Conn= ConfigurationManager.ConnectionStrings["con"].ConnectionString;
       return Conn;
    }
}