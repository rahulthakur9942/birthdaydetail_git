﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="BirthdayDetailForm2.aspx.cs" Inherits="BirthdayDetailForm" %>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">
     <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Title Page-->
    <title>BirthDay</title>

    <!-- Icons font CSS-->
    <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="css/main.css" rel="stylesheet" media="all">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
  
  <style>
     .dataTables_filter input { height: 40px }
      #GVbirthday_length {
          display:none
      }
      .input--style-4 {
              padding: 0px 45px;
      }
      #GVbirthday_filter {
          background: #999;
      }
      .title {
          font-size: 24px;
          color: #525252;
          font-weight: 400;
          margin-bottom: 15px;
      }
      .label {
          color: black;
      }
      #ul_months td {
          background-color: bisque;
      }
  </style>
</head>

<body>
     <form runat="server" id="form1">
    <div class="page-wrapper font-poppins">
        <div class="wrapper wrapper--w680">
            <div class="card card-4">
                <div class="card-body" id="DivNew" visible="true" runat="server" style="background-color: #f3897d;">
                    
                   
                  
                      <div>
                         <h2 class="title">Add BirthDay</h2>
                           <asp:Label ID="Labeltotal" runat="server" CssClass="Labelcss"  Font-Bold="True"  Width="100px"></asp:Label>
                             <div class="row row-space">
                            <div class="col-12" style="padding: 0;">
                                       <div>
                                 <div class="table-responsive">

                            <table id="ul_months" style="text-align: center;" class="table table-bordered table-striped">
                                <tr>
                                <td id="1"><a href="#">Jan</a></td>
                                <td id="2"><a href="#">Feb</a></td>
                                <td id="3"><a href="#">Mar</a></td>
                              <td id="4"><a href="#">Apr</a></td>
                                <td id="5"><a href="#">May</a></td>
                                <td id="6"><a href="#">Jun</a></td>  </tr>
                                <tr>
                              <td id="7"><a href="#">Jul</a></td>
                                <td id="8"><a href="#">Aug</a></td>
                                <td id="9"><a href="#">Sep</a></td>
                              <td id="10"><a href="#">Oct</a></td>
                                <td id="11"><a href="#">Nov</a></td>
                                <td id="12"><a href="#">Dec</a></td>

                                </tr></table>

                             </div>
                                           </div>
                                </div>
                                 </div>
                        <div class="row row-space">
                            <div class="col-12">
                                       <div class="input-group">
                                    <label class="label">Birthday</label>
                                    <div class="input-group-icon">
                                   
                                         <asp:TextBox ID="txtDOB"  runat="server" ClientIDMode="Static" AutoComplete="off"  class="input--style-4 js-datepicker" ></asp:TextBox>
                                        <i class="zmdi zmdi-calendar-note input-icon js-btn-calendar"></i>
                                    </div>
                                </div>

                             
                            </div>
                                    <div class="col-12">
                                       <div class="input-group">
                                    <label class="label">Contact#</label>
                                    <div class="input-group-icon">
                                   
                                         <asp:TextBox ID="txtContectNumber" runat="server" AutoComplete="off" ClientIDMode="Static" class="input--style-4"></asp:TextBox>

                                    </div>
                                </div>

                             
                            </div>
                     
                        </div>

                             <div class="row row-space">
                                             <div class="col-md-12" id="dv_exists" style="display:none">
                                <div class="form-group">
                                                       <div class="table-responsive">

                            <table style="text-align: center;" class="table table-bordered table-striped">
                                <thead>
                                    <th>Name</th>
                                    <th>DOB</th>
                                     <th>Edit</th>
                                    <th>Delete</th>
                               

                                </thead>
                                     <tbody id="tbl_gvdisplay">
                                   
                                     </tbody>
                                            

                            </table>

                        </div>
                                </div>
                            </div>
                                 </div>
                 <div class="row row-space">
                                     <div class="col-12">
                                       <div class="input-group">
                                    <label class="label">Name</label>&nbsp; &nbsp; <label id="lblerror" style="color:red;font-weight: 800;font-size: smaller;"></label>
                                    <div class="input-group-icon">
                                   
                                         <asp:TextBox  ID="txtName" runat="server" AutoComplete="off" ClientIDMode="Static" class="input--style-4"></asp:TextBox>
                                            
                                    </div>
                                </div>

                             
                            </div>
                                                                   
                                
                                
                                               
                       <div class="col-md-12">
                            
                                    <label for="ContentPlaceHolder1_ddLanguage" style="font-size: 16px;color: black;text-transform: capitalize;display: block;margin-bottom: 5px;">Select Language</label>
                                    <asp:DropDownList ID="ddLanguage" runat="server" class="form-control" style="height: 45px;background-color: #fafafa;">
                                        <asp:ListItem Value="English" Text="English"></asp:ListItem>
                                     
                                        <asp:ListItem Value="Punjabi" Text="Punjabi"></asp:ListItem>
                                    </asp:DropDownList>
                              
                            </div>
                              
                             

                             
                       
                                          <div class="col-12"  style="display:none">
                                       <div class="input-group">
                                    <label class="label">DOA</label>
                                    <div class="input-group-icon">
                                   
                                         <asp:TextBox ID="txtDOA" runat="server" AutoComplete="off" ClientIDMode="Static"  class="input--style-4"></asp:TextBox>

                                    </div>
                                </div>

                             
                            </div>
                     </div>
                                <div class="row row-space">
                                                      <div class="col-12"  style="display:none">
                                       <div class="input-group">
                                    <label class="label">City</label>
                                   <div class="rs-select2">
                                   
                                        
                                        <asp:DropDownList ID="ddCity" runat="server"  class="form-control"></asp:DropDownList>
                                    </div>
                                </div>

                             
                            </div>

                         
                                    </div>

                        <div class="p-t-15">
                           
                             <asp:Button ID="Submit" runat="server" class="btn btn--radius-2 btn--blue" Text="Submit" OnClick="Submit_Click" ClientIDMode="Static"></asp:Button>
                        </div>
                          <div class="p-t-15">
                           
                                                               <asp:Button ID="btnSearch" runat="server" class="btn btn-primary btn-block" Text="Search" OnClick="btnSearch_Click" UseSubmitBehavior="False"></asp:Button>
                        </div>
                            </div>
              
                   
                </div>   
                             <div id="DivEditData" visible="false" runat="server">
                <div class="card card-register mx-auto" style="background-color: #f7f7f7">
                    <div class="card-header">Search Birthday</div>
                    <div>
                        <div class="row">
                                       <div class="table-responsive"  style="height: 91px;">

                            <table id="ul_months2" style="text-align: center;" class="table table-bordered table-striped">
                                <tr>
                                <td>
                                    <asp:LinkButton ID="LinkButton1"  CssClass="1" runat="server" CommandName="jan" OnClick="LinkButton1_Click">Jan</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton2" CssClass="2" runat="server" OnClick="LinkButton1_Click">Feb</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton3"  CssClass="3" runat="server" OnClick="LinkButton1_Click">Mar</asp:LinkButton></td>
                              <td><asp:LinkButton ID="LinkButton4"  CssClass="4" runat="server" OnClick="LinkButton1_Click">Apr</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton5"  CssClass="5" runat="server" OnClick="LinkButton1_Click">May</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton6"  CssClass="6" runat="server" OnClick="LinkButton1_Click">Jun</asp:LinkButton></td>  </tr>
                                <tr>
                              <td><asp:LinkButton ID="LinkButton7"  CssClass="7" runat="server" OnClick="LinkButton1_Click">Jul</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton8"  CssClass="8" runat="server" OnClick="LinkButton1_Click">Aug</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton9"  CssClass="9" runat="server" OnClick="LinkButton1_Click">Sep</asp:LinkButton></td>
                              <td><asp:LinkButton ID="LinkButton10"  CssClass="10" runat="server" OnClick="LinkButton1_Click">Oct</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton11"  CssClass="11" runat="server" OnClick="LinkButton1_Click">Nov</asp:LinkButton></td>
                                <td><asp:LinkButton ID="LinkButton12"  CssClass="12" runat="server" OnClick="LinkButton1_Click">Dec</asp:LinkButton></td>

                                </tr></table>

                             </div>
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GVbirthday" class="table table-bordered table-striped" runat="server" AutoGenerateColumns="False" OnRowCommand="GVbirthday_RowCommand" OnPreRender="GVbirthday_PreRender">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Name/Contact" SortExpression="ddlAirlinePenality">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name")+ "<br/> " + Eval("ContectNumber")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:BoundField DataField="ID" HeaderText="ID" ReadOnly="True" />--%>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandName="Select" CommandArgument='<%#Eval("ID") %>' Font-Bold="True" ForeColor="Blue"><i class="fa fa-pencil"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnDelete" runat="server" CommandName="del" CommandArgument='<%#Eval("ID") %>' OnClientClick="return confirm('Do you really want?');" Font-Bold="True" ForeColor="Blue"><i class="fa fa-trash"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                       
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
               
              
            </div>
        </div>
        <asp:HiddenField ID="hdn_id" runat="server" />
    </div>
      
     </form>
    <!-- Jquery JS-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="vendor/select2/select2.min.js"></script>
    <script src="vendor/datepicker/moment.min.js"></script>
    <script src="vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
   

    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
 

    <script type="text/javascript">

    $(document).ready(function () {
        $("#GVbirthday").dataTable();
        $(document.body).on('click', '.btneditrec', function (e) {
            var currentrow = $(this).closest('tr');
            lang = currentrow.find('.cls_lang').text();
            id = currentrow.find('.cls_id').text();
            dob = currentrow.find('.cls_dob').text();
            contact = currentrow.find('.cls_contact').text();
            name = currentrow.find('.cls_name').text();

            $("#txtDOB").val(dob);
            $("#txtContectNumber").val(contact);
            $("#txtName").val(name);
            $("#ddLanguage").val(lang); 
            $("#hdn_id").val(id);
            $("#dv_exists").hide();
            $("#Submit").val("Update");
            $("#Submit").removeAttr('disabled');
            $("#lblerror").text("");
        });
 
        $(document.body).on('click', '.btnremove', function (e) {
            if (confirm("Are you sure you?")) {
                var currentRow = $(this).closest("tr");

                id = currentRow.find('.cls_id').text();

                $.ajax({
                    type: "POST",
                    url: "BirthdayDetailForm.aspx/del_rec",
                    contentType: "application/json",
                    data: '{ "id": "' + id + '"}',
                    dataType: "json",
                    success: function () {
                        currentRow.closest('tr').remove();
                        alert("Record deleted Successfully");


                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        var obj = jQuery.parseJSON(xhr.responseText);
                        alert(obj.Message);
                    },
                    complete: function () {



                    }

                });
            }
        });
        $("#txtName").keyup(function () {
           
            var contactno = $("#txtContectNumber").val();
            var contactname = $(this).val();

            $.ajax({
                type: "POST",
                url: "BirthdayDetailForm.aspx/chk_contact_dtl",
                contentType: "application/json",
                data: '{ "contactno": "' + contactno + '","contactname": "' + contactname + '"}',
                dataType: "json",
                success: function (data) {
                    var obj = jQuery.parseJSON(data.d);
                    if(obj.rtnval=="1")
                    {
                      
                        if ($('#Submit').val() == 'Submit')
                        {
                            $("#lblerror").text("**Record Already Exist.");
                            $("#Submit").attr('disabled','disabled');
                        }
                    }
                    else {
                        $("#lblerror").text("");
                        $("#Submit").removeAttr('disabled');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

            

                }

            });

       
           
        });


        $("#txtContectNumber").keyup(function () {

            chk_contactno($(this).val());

        });

        function chk_contactno(contact_no)
        {

            var contactno = $("#txtContectNumber").val();
            $.ajax({
                type: "POST",
                url: "BirthdayDetailForm.aspx/chk_contactno",
                contentType: "application/json",
                data: '{ "contactno": "' + contactno + '"}',
                dataType: "json",
                success: function (data) {
                    var obj = jQuery.parseJSON(data.d);
                    if (contact_no != "")
                        {
                        if (obj.tableoption.length != 0) {

                       
                        $("#tbl_gvdisplay").html(obj.tableoption);
                        $("#dv_exists").show();

                    }
                    else {
                        $("#dv_exists").hide();
                    }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    var obj = jQuery.parseJSON(xhr.responseText);
                    alert(obj.Message);
                },
                complete: function () {

            

                }

            });

        }
        $("#ul_months td").on("click", function () {
            var currentTime = new Date();
            var year = currentTime.getFullYear();
            var month = $(this).attr("id");

            $("#txtDOB").val('01/' + month + '/' + year + '');
        
            $("#txtDOB").click(function () {
                $(".monthselect").val(month - 1);
               
            });
      
            $("#txtDOB").click();

        });

   
    });
</script>
 


</html>
<!-- end document-->
