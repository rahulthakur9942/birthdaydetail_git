﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TodayBirthdaysList.aspx.cs" Inherits="TodayBirthdaysList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

    <!-- Page level plugin CSS-->


    <!-- Custom styles for this template-->
    <link href="css/sb-admin.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

 <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    
   


    <style>
        th {
            text-align: center;
        }

        td {
            text-align: center;
        }

        .Labelcss
        {
            margin-left:70px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="container" style="margin-top: -48px; padding-left:0px; padding-right:0px;">
            <div id="DivEditData" runat="server">
                <div class="card card-register mx-auto mt-5" style="background-color: #2b968c">
                    <div class="card-header">Today Birthday List</div>
                    <div class="card-body">
                        <div class="row">
                          
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <asp:LinkButton ID="btn_export" runat="server" OnClick="btn_export_Click"><i class="fa fa-file-excel-o" aria-hidden="true"  style="font-size: 25px;color: cornsilk;"></i></asp:LinkButton>
                                   
                                    <asp:GridView ID="Gvdata" runat="server" OnPreRender="Gvdata_PreRender" AutoGenerateColumns="False">
                                         <Columns>
                                            <asp:TemplateField HeaderText="Name/Contact" SortExpression="ddlAirlinePenality">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%#Eval("Name")+ "<br/> " + Eval("ContectNumber")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                                    <asp:BoundField DataField="DOB" HeaderText="DOB"/>
                                          <asp:BoundField DataField="Status" HeaderText="Status"/>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>

<%--<script src="vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="vendor/datatables/jquery.dataTables.js"></script>--%>
<script type="text/javascript">
    $(document).ready(function () {
        $("#Gvdata").dataTable({
            "bPaginate": false,
            "bSort": true
        });
    });
</script>
     <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js" type="text/javascript"></script>
</html>
