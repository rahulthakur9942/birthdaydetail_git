﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;

public partial class BirthdayDetailForm : System.Web.UI.Page
{
    SqlConnection con;
    int Total = 0;
  
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);

        string conprop = con.ConnectionString;
    
        if (IsPostBack == false)
        {
            BindCity();
            BindGrid();
            Total = GVbirthday.Rows.Count;
            Labeltotal.Text =" Records: " + Total;
        }
     }

    protected void Submit_Click(object sender, EventArgs e)
    {
       
        if (hdn_clickval.Value == "1")
        {
            InsertUpdate();
            Response.Redirect("BirthdayDetailForm.aspx");
            //txtName.Text = " ";
            //txtContectNumber.Text = " ";
            //txtDOB.Text = " ";
            //txtDOA.Text = " ";
            //ddCity.SelectedItem.Value = "1";
            //ddLanguage.SelectedItem.Value = "1";
            BindGrid();
            BindCity();
            Total = GVbirthday.Rows.Count;
            Labeltotal.Text = "Records:" + Total;
        }
        else
        {

            Response.Write("<script>alert('Please Select DOB')</script>");
        }
    }
    public void InsertUpdate()
    {
        if (!string.IsNullOrEmpty(hdn_id.Value.Trim()))
        {
            ViewState["ID"] = hdn_id.Value.Trim();
        }
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_birthday", con);
        cmd.Parameters.AddWithValue("@id", ViewState["ID"]);
        cmd.Parameters.AddWithValue("@name", txtName.Text);
        cmd.Parameters.AddWithValue("@contectnumber", txtContectNumber.Text);
        cmd.Parameters.AddWithValue("@dob", txtDOB.Text);

        if (txtDOA.Text == "00")
        {
            cmd.Parameters.AddWithValue("@doa", "01-01-1900");
        }
        else
        {
            cmd.Parameters.AddWithValue("@doa", txtDOA.Text);
        }
        cmd.Parameters.AddWithValue("@city", ddCity.SelectedItem.Text);
        cmd.Parameters.AddWithValue("@language", ddLanguage.SelectedItem.Text);
        cmd.Parameters.AddWithValue("@req", "insertupdate");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.ExecuteNonQuery();
        if (Submit.Text == "Submit")
        {
            Response.Write("<script>alert('Record Inserted Successfully')</script>");
        }
        else
        {
            Response.Write("<script>alert('Record Updated Successfully')</script>");
        }
        con.Close();

        //Submit.Text = "Submit";
    }
    public void BindCity()
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("Select City_Name,City_ID from Tbl_City order by City_Name ", con);
        DataTable dt = new DataTable();
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        adp.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            ddCity.DataSource = dt;
            ddCity.DataTextField = "City_Name";
            ddCity.DataValueField = "City_ID";
            ddCity.DataBind();
        }
        con.Close();
    }
        protected void LinkButton1_Click(object sender, EventArgs e)
    {
        string month = (sender as LinkButton).CssClass;
        SqlCommand cmd = new SqlCommand("strp_birthday", con);
        cmd.Parameters.AddWithValue("@req", "get_bd_monthwise");
        cmd.Parameters.AddWithValue("@month", month);
        cmd.CommandType = CommandType.StoredProcedure;
        DataTable dt = new DataTable();
        SqlDataAdapter adb = new SqlDataAdapter(cmd);
        adb.Fill(dt);
        GVbirthday.DataSource = dt;
        GVbirthday.DataBind();

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        DivNew.Visible = false;
        DivEditData.Visible = true;
        //BindGrid();
    }
    public void BindGrid()
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_birthday", con);
        cmd.Parameters.AddWithValue("@req", "getdata");
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GVbirthday.DataSource = dt;
            GVbirthday.DataBind();
        }
        con.Close();
    }
  
    protected void GVbirthday_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Submit.Text = "Update";
        String ID = Convert.ToString(e.CommandArgument);
         ViewState["ID"] = ID;

        if (e.CommandName == "Select")
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_birthday", con);
            cmd.Parameters.AddWithValue("@req", "getdata_edit");
            cmd.Parameters.AddWithValue("@id", ID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                txtName.Text = rdr["Name"].ToString();
                txtContectNumber.Text = rdr["ContectNumber"].ToString();
                // DateTime dob = Convert.ToDateTime(rdr["DOB"]);
                //txtDOB.Text = dob.ToShortDateString();
               txtDOB.Text = rdr["DOB"].ToString();
                hdn_clickval.Value = "1";

                if (rdr["DOA"].ToString() == "01-01-1900 00:00:00")
                {
                    txtDOA.Text = "00";
                }
                else
                {
                    txtDOA.Text = rdr["DOA"].ToString();
                    //DateTime doa = Convert.ToDateTime(rdr["DOA"]);
                    //txtDOA.Text = doa.ToShortDateString();
                }
                   
                ddCity.SelectedItem.Text = rdr["City"].ToString();
                ddLanguage.SelectedItem.Text = rdr["Lang"].ToString();
            }
            con.Close();
        }
        else if (e.CommandName == "del")
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_birthday", con);
            cmd.Parameters.AddWithValue("@req", "datadelete");
            cmd.Parameters.AddWithValue("@id", ID);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            Response.Write("<script>alert('Record deleted Successfully')</script>");
            con.Close();
            BindGrid(); 
            BindCity();
            Total = GVbirthday.Rows.Count;
            Labeltotal.Text = "Records:" + Total;
           
        }
        DivNew.Visible = true;
        DivEditData.Visible = false;
    }
    protected void GVbirthday_PreRender(object sender, EventArgs e)
    {
        // You only need the following 2 lines of code if you are not 
        // using an ObjectDataSource of SqlDataSource
       // BindGrid();

        if (GVbirthday.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            GVbirthday.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            GVbirthday.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            GVbirthday.FooterRow.TableSection = TableRowSection.TableFooter;
        }

    }

    [WebMethod]
    public static void del_rec(string id)
    {
        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_birthday", con);
            cmd.Parameters.AddWithValue("@req", "datadelete");
            cmd.Parameters.AddWithValue("@id", id);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            con.Close();

        }
    }
    [WebMethod]
    public static string chk_contactno(string contactno)
    {

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            StringBuilder StringBuilder = new StringBuilder();
            SqlCommand cmd = new SqlCommand("strp_birthday", con);
            cmd.Parameters.AddWithValue("@req", "getcontact_dtl");
            cmd.Parameters.AddWithValue("@contactno", contactno);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string[] date = dt.Rows[i].ItemArray[3].ToString().Split(' ');
                StringBuilder.Append(string.Format("<tr><td class='cls_contact' style='display:none'>" + dt.Rows[i].ItemArray[2] + "</td><td class='cls_lang' style='display:none'>" + dt.Rows[i].ItemArray[6] + "</td><td class='cls_id' style='display:none'>" + dt.Rows[i].ItemArray[0] + "</td><td class='cls_name'>" + dt.Rows[i].ItemArray[1] + "</td><td class='cls_dob'>" + date[0] + "</td><td  class='cls_edit'><i class='fa fa-pencil btneditrec' style='color: green;font-size: 20px;'></i></td><td class='cls_remove'><i class='fa fa-trash btnremove' style='color: red;font-size: 20px;'></i></td></tr>"));

            }
            var Json = new
            {

                tableoption = StringBuilder.ToString()
            };
            return ser.Serialize(Json);

        }
    }

    [WebMethod]
    public static string chk_contact_dtl(string contactno, string contactname)
    {

        using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString))
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            string rtnval = "0";
            SqlCommand cmd = new SqlCommand("strp_birthday", con);
            cmd.Parameters.AddWithValue("@req", "chk_contact_dtl");
            cmd.Parameters.AddWithValue("@contactno", contactno);
            cmd.Parameters.AddWithValue("@name", contactname);
            cmd.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            SqlDataAdapter adb = new SqlDataAdapter(cmd);
            adb.Fill(dt);
            if (dt.Rows.Count > 0)
            {

                rtnval = "1";
            }
            var Json = new
            {

                rtnval
            };
            return ser.Serialize(Json);

        }
    }
}