﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web.Services;

public partial class MessageSetting : System.Web.UI.Page
{
    SqlConnection con;
    protected void Page_Load(object sender, EventArgs e)
    {
        con = new SqlConnection(ConfigurationManager.ConnectionStrings["con"].ConnectionString);
        if(IsPostBack==false)
        {
            BindGrid();
        }
    }

    protected void Submit_Click(object sender, EventArgs e)
    {
        InsertUpdate();
    }
    public void InsertUpdate()
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_birthday", con);
        cmd.Parameters.AddWithValue("@id", ViewState["ID"]);
        cmd.Parameters.AddWithValue("@birthdaymesssage", txtBirthday.Text);
        cmd.Parameters.AddWithValue("@anniversarymesssage", txtAnniversary.Text);
        cmd.Parameters.AddWithValue("@req", "InsertUpdateMsg");
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.ExecuteNonQuery();
        con.Close();
        Response.Write("<script>alert('Message Inserted Successfully')</script>");
        Response.Redirect("MessageSetting.aspx");
        BindGrid();
    }
    public void BindGrid()
    {
        con.Open();
        SqlCommand cmd = new SqlCommand("strp_birthday", con);
        cmd.Parameters.AddWithValue("@req", "GetMessage");
        cmd.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter adp = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        adp.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            GVbirthday.DataSource = dt;
            GVbirthday.DataBind();
         
        }
        con.Close();
    }

    protected void GVbirthday_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        String ID = Convert.ToString(e.CommandArgument);
        ViewState["ID"] = ID;

        if (e.CommandName == "Select")
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_birthday", con);
            cmd.Parameters.AddWithValue("@req", "GetMessageEdit");
            cmd.Parameters.AddWithValue("@id", ID);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader rdr = cmd.ExecuteReader();
            if (rdr.Read())
            {
                txtBirthday.Text = rdr["Birthday_Message"].ToString();
                txtAnniversary.Text = rdr["Anniversary_Message"].ToString();
            }
            con.Close();
        }
        else if (e.CommandName == "del")
        {
            con.Open();
            SqlCommand cmd = new SqlCommand("strp_birthday", con);
            cmd.Parameters.AddWithValue("@req", "MessageDelete");
            cmd.Parameters.AddWithValue("@id", ID);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.ExecuteNonQuery();
            con.Close();
        }
        BindGrid();
        Submit.Text = "Update";
    }

}